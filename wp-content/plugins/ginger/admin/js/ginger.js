
function select_privacy_page(){

    document.getElementById('privacy_page_select').disabled=false;
    document.getElementById('new_page_privacy').style.display='none';
    document.getElementById('new_page_privacy').style.display='none';
}

function new_privacy_page(){

    document.getElementById('privacy_page_select').disabled=true;
    document.getElementById('new_page_privacy').style.display='inline';
    document.getElementById('new_page_privacy').style.display='inline';
}
function disable_text_banner_button(id){

    document.getElementById(id).disabled=true;
    document.getElementById('new_page_privacy').style.display='inline';
    document.getElementById('new_page_privacy').style.display='inline';
}
function enable_text_banner_button(id){

    document.getElementById(id).disabled=false;
    document.getElementById('new_page_privacy').style.display='inline';
    document.getElementById('new_page_privacy').style.display='inline';
}

function en_dis_able_text_banner_button(id,id_text,id_img){

    var status=document.getElementById(id).checked;


    if (status){

        document.getElementById(id_text).disabled=false;
        document.getElementById(id_img).src='../wp-content/plugins/ginger/img/ok.png';

    }else if (!status){

        document.getElementById(id_text).disabled=true;
        document.getElementById(id_img).src='../wp-content/plugins/ginger/img/xx.png';


    }

}

function en_dis_able_add_on(id,id_img,id_text){


    var status=document.getElementById(id).checked;


    if (status){
        if (id!='google_analytics_status') {
            document.getElementById(id_text).disabled = false;
        }
        document.getElementById(id_img).src='../wp-content/plugins/ginger/img/ok.png';

    }else if (!status){

        document.getElementById(id_text).disabled=true;
        document.getElementById(id_img).src='../wp-content/plugins/ginger/img/xx.png';
    }

}

function ginger_share_data(){

    jQuery(document).ready(function () {
        jQuery('#ginger_share_data').on('click', function (e) {
            e.preventDefault();
            const email_auth = jQuery(this).data('email');
            const url = 'http://ginger.manafactory.it/user/create';
            const data = {
                email: email_auth,
            };

            const ginger_statistic_share = jQuery('#ginger_informations_share').val();
            const ginger_informations_share = jQuery('#ginger_informations_share').val();



            const request = jQuery.ajax({
                url: url,
                type: 'POST',
                data: JSON.stringify(data),
                processData: false,
                contentType: 'application/json',
                success: function(data, textStatus, request){
                    const token = request.getResponseHeader('x-auth');
                    const data_token = {
                        'action': 'ginger_save_info',
                        ginger_token: token,
                        ginger_share_data: true,
                        ginger_email_share_data: email_auth,
                        ginger_statistic_share: ginger_statistic_share,
                        ginger_informations_share: ginger_informations_share
                    };
                    jQuery.post(ajaxurl, data_token, function (response) {

                        const data_token = {
                            'action': 'ginger_share_to_ws',
                        };
                        jQuery.post(ajaxurl, data_token, function (response) {
                            tb_remove();
                        });
                    });
                },
                error: function (request, textStatus, errorThrown) {
                    alert(request.getResponseHeader('some_header'));
                }
            });
        });
    });
}





if(ClipboardJS.isSupported()){
    const clipboard = new ClipboardJS('.btn');
    clipboard.on('success', function(e) {
        e.trigger.innerHTML = 'Copied!';
        setTimeout(function()  {
            e.trigger.innerHTML = 'Copy to clipboard';
            e.clearSelection();
        }, 1000, e);
    });

    clipboard.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });
}
