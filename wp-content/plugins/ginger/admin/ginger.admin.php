<?php

/**
 *
 */
add_action('admin_footer', 'ginger_save_javascript');
list($tab, $key) = tabSelection();
equeueScript($tab);


$options = get_option($key);
if ($key == 'ginger_policy'):

    $options = get_option($key);
    // recupero la option per il disable click out e scroll in privacy policy page
    $options2 = get_option($key . '_disable_ginger');
    $options_disable_logger = get_option($key . '_disable_logger');

endif;
?>

<div class="wrap">
    <h2>Ginger - EU Cookie Law</h2>
    <hr>
    <h2 class="nav-tab-wrapper">
        <a href="admin.php?page=ginger-setup"
           class="nav-tab <?php echo (($_GET["page"] == 'ginger-setup') && ((!isset($_GET["tab"]) || $_GET["tab"] == "") || (isset($_GET["tab"]) && $_GET["tab"] == "general"))) ? 'nav-tab-active' : ''; ?>"><?php _e("General Configuration", "ginger"); ?></a>
        <a href="admin.php?page=ginger-setup&tab=banner"
           class="nav-tab <?php echo (($_GET["page"] == 'ginger-setup') && (isset($_GET["tab"]) && $_GET["tab"] == "banner")) ? 'nav-tab-active' : ''; ?>"><?php _e("Banner Setup", "ginger"); ?></a>
        <a href="admin.php?page=ginger-setup&tab=policy"
           class="nav-tab <?php echo (($_GET["page"] == 'ginger-setup') && (isset($_GET["tab"]) && $_GET["tab"] == "policy")) ? 'nav-tab-active' : ''; ?>"><?php _e("Privacy Policy", "ginger"); ?></a>
        <a href="admin.php?page=ginger-setup&tab=admin-permission"
           class="nav-tab <?php echo (($_GET["page"] == 'ginger-setup') && (isset($_GET["tab"]) && $_GET["tab"] == "admin-permission")) ? 'nav-tab-active' : ''; ?>"><?php _e("Admin Permission", "ginger"); ?></a>
        <a href="admin.php?page=ginger-setup&tab=shortcode"
           class="nav-tab <?php echo (($_GET["page"] == 'ginger-setup') && (isset($_GET["tab"]) && $_GET["tab"] == "shortcode")) ? 'nav-tab-active' : ''; ?>"><?php _e("Shortcode", "ginger"); ?></a>
        <?php do_action("ginger_add_tab_menu"); ?>
        <a href="admin.php?page=ginger-setup&tab=need_more"
           class="nav-tab <?php echo (($_GET["page"] == 'ginger-setup') && (isset($_GET["tab"]) && $_GET["tab"] == "need_more")) ? 'nav-tab-active' : ''; ?>" style="float: right"><?php _e("Need More?", "ginger"); ?></a>
        <a href="admin.php?page=ginger-setup&tab=ginger_policy"
           class="nav-tab <?php echo (($_GET["page"] == 'ginger-setup') && (isset($_GET["tab"]) && $_GET["tab"] == "ginger_policy")) ? 'nav-tab-active' : ''; ?>" style="float: right"><?php _e("Ginger Policy", "ginger"); ?></a>
    </h2>
    <?php if ($tab != 'shortcode'): ?>
    <form id="ginger_save_data" method="post"
          action="admin.php?page=<?php echo $_GET["page"]; ?><?php if (isset($tab)) echo '&tab=' . $tab; ?>" <?php echo 'class="repeater"'; ?>>
        <?php wp_nonce_field('save_ginger_options', 'ginger_options'); ?>
        <?php endif; ?>
        <?php switch ($tab) :
            case "general":
                include('partial/general.php');
                break;
            case "banner":
                include('partial/banner.php');
                break;
            case "policy":
                include('partial/policy.php');
                break;
            case "admin-permission":
                include('partial/admin-permission.php');
                break;
            case "shortcode":
                include('partial/shortcode.php');
                break;
            case "ginger_policy":
                include('partial/ginger-cookie-policy.php');
                break;
	        case "need_more":
		        include('partial/ginger-more.php');
		        break;
            default:
                include('partial/general.php');
        endswitch; ?>
        <?php if ($tab != 'shortcode' && $tab != 'ginger_policy' && $tab != 'need_more'): ?>
        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary"
                   value="<?php _e("Save Changes", "ginger"); ?>">
        </p>
    </form>
<?php endif; ?>
</div>
